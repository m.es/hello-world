
import 'package:flutter/material.dart';

import 'package:flutter/scheduler.dart' show timeDilation;


class LoginScreen extends StatefulWidget {


  @override
  State<StatefulWidget> createState() => new LoginScreenState();

  }




class LoginScreenState extends State<LoginScreen> with SingleTickerProviderStateMixin {
  late AnimationController _loginButtoncontroller;

  final _myTextFieldController = new TextEditingController();


  final _formKey = GlobalKey<FormState>();
  late String _emailValue;
  late String _passwordValue;

  emailOnSaved(String value) {
    print(value + 'asfasfa');

  }

  passwordOnSaved(String value) {
    print(value);
  }




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loginButtoncontroller = new AnimationController(vsync: this , duration: new Duration(milliseconds: 3000));
  }


  @override
  void dispose() {
    // TODO: implement dispose
    _loginButtoncontroller.dispose();
    super.dispose();
  }




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    timeDilation = .4 ;
    var page = MediaQuery.of(context).size;

    return new Scaffold(
      resizeToAvoidBottomInset: false,
      body: new Container(
        decoration: new BoxDecoration(
          gradient: new  LinearGradient(
              colors: <Color>[
                const Color(0xff2c5364),
                const Color(0xff0f2027)
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter
          )
        ),
        child: new Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            new Opacity(
              opacity: .1,
              child: new Container(
                width: page.width,
                height: page.height,
                decoration: new BoxDecoration(
                    image: new DecorationImage(
                        image: new AssetImage("assets/images/icon_background.png"),
                        repeat: ImageRepeat.repeat
                    )
                ),
              ),
            ),
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //new FormContainer(
                    //formKey : _formKey,
                    //emailOnSaved : emailOnSaved,
                    //passwordOnSaved : passwordOnSaved

                //),
                new TextField(
                  onChanged: (String value) {
                    print(value);
                  }
                ),
                new TextButton(
                  onPressed: null,
                  child: new Text(
                    "آیا هیچ اکانتی ندارید؟ عضویت",
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      letterSpacing: 0.5,
                      color: Colors.white,
                      fontSize: 14
                    ),
                  ))
              ],
            ),
            new GestureDetector(
              onTap: () async {
                if(_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  sendDataForLogin();
                }
                //await _loginButtoncontroller.forward();
                //await _loginButtoncontroller.reverse();
              },

            )

          ],
        ),
      ) ,
    );
  }

   sendDataForLogin() async {
       await _loginButtoncontroller.animateTo(0.150);
    // await _loginButtoncontroller.forward();
    // await _loginButtoncontroller.reverse();

  }

}